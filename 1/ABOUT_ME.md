Hi,

I'm Rafał, Software Developer from Poland, currently living in Thailand.
I work in agile methodologies (scrum).
I had an oportunity to work with Ruby (Rails), Postgres, RSpec, Sidekiq, Ember.js,
React, GraphQL, ElasticSearch, Heroku, Git, Bootstrap, nmp, OpenCV and Hexo :)
I also have basic experience with C++.

I enjoy building, designing things and automate the process when possible.
I have an interest in travel, culture, and food.
I read biographies / personal-development / marketing / mystery books.

I'm strong believer of doing more by doing less (and deliver more efficiently).

I've been working on various projects:

## Commercial:

#### Line Mobile (Mobile network service that competes with well-known networks such as AIS, dtac and Truemove H)

  * Helped improve existing payments integration
  * Integrated Mobile Banking Payment
  * Debugging code, track system logs, bugfixes

#### Creatubbles (Content sharing social platform for kids, parents and teachers)

  * Worked in fully distributed team (SCRUM)
  * Single Page Applications (SPA) based on Ember.js
  * Implemented reusable UI components for crucial features, practiced TDD
  * Helped improve test coverage for frontend code
  * Internalizations

#### Time Tracker (Tool dedicated for Developers / Project Managers)

  * Improve test reliabliity for complex business logic (RSpec)
  * Eliminate edge cases for reports computation
  * Work on building new features (Ruby on Rails, Ember.js)
  * Improving structure and performance of existing code

#### Send24 (parcel delivery)

  * Worked as a Junior Developer on complex plaform.
  * bugfixes, User Onbording

#### Another projects:

  * Develop new functionalities for SaaS product (Ruby on Rails, popular gems)
  * Payments integration, accounts, invitations, multitenant database
  * Pricing Plans

## Freelance

#### Roboticket (Sport events management app)

  * Build Dashboard for managing sport events (RoR)
  * Build API for mobile application

#### Kiidu (Booking nanny on demand)

  * Develop GraphQL API for React app.
  * Used Semantic UI to build interfaces.

## Side projects

  * Build system for managing squash leagues based on real rules from local club (http://www.racketmanager.com/)
  * Build simple Todo app with categories, priorities and color assigning (https://storeit.herokuapp.com/)
  * SEO Adwords Report Tool: https://seo-reports.herokuapp.com/reports
