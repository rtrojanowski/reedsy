# Book
  # - author
  # - title
  # - published_on
  # - genre_1, genre_2, genre_3

# User
  # - name

class BookRecommender
  include Predictor::Base

  input_matrix :genres, weight: 2.0
  input_matrix :users, weight: 1.0
end

recommender = BookRecommender.new

# Genres and their books. This will lead to sets like:

"fiction" -> "book-1", "book-2",
"mystery" -> "book-2", "book-3"
"self-help" -> "book-4", "book-5"

recommender.genres.add_to_set("genre-1", "book-1", "book-2")
recommender.process_items!("book-1", "book-2")

# We could do something like:

Book.find_each do |book|
  book.genres.each do |genre|
    recommender.add_to_matrix!(:genres, genre, book.id)
  end
end

# Users that viewed this also liked ...

recommender.similarities_for("book-1")

# User upvoted book-1 and book-2. Let's see what else they might like...

recommender.users.add_to_set("user-1", "book-1", "book-2")
recommender.process_items!("book-1", "book-2")

recommender.predictions_for("user-1", matrix_label: :users)