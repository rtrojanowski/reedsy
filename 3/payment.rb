# Implement a Payment Factory to process payments with multiple adapter (Stripe, Paypal, etc...). Your code should respect SOLID principles as much as possible. Provide only the design without any concrete methods implementation.

class PaymentService

  def initialize(amount:, payment_gateway:)
    @amount = amount
    @payment_gateway = payment_gateway
  end

  def pay
    @payment_gateway.pay(price)
  end

  def price
    amount + vat
  end

  def vat
    0.23 * amount
  end
end

class PaymentGateway
  def pay(amount)
    raise 'must be implemented'
  end
end

class PayPal < PaymentGateway

  def pay(amount)
    PayPalApi.charge!(amount)
  end

end

class Stripe < PaymentGateway
  def pay(amount)
    StripeApi.new(amount).charge
  end
end