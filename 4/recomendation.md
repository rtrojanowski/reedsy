### With the previous data schema (question 2), with a list of genres for each books, how would you build a recommendation system. What kind of dependencies, tools or algorithms you would like to use?

I would use this gem: https://github.com/Pathgather/predictor
It uses https://en.wikipedia.org/wiki/Jaccard_index to determine similarities between items.