class Feed

  attr_reader :user

  def initialize(user)
    @user = user
  end

  def retrieve(refresh = false)
    @collection = user_books

    if refresh && !user.feed_read_at.nil?
      @collection.select! {|item| item.published_on >= user.feed_read_at }
    end

    @collection = @collection.sort_by {|item| -item.published_on.to_time.to_i }

    user.feed_read_at = Date.today

    @collection
  end

  def refresh
    retrieve(true)
  end

  def unread_count
    # that method doesn't work perfectly
    user_books.select do |book|
      book.published_on > user.feed_read_at
    end.size rescue user_books.size
  end

  def user_books
    user_books = Set[*find_by_author(user.authors)]
    user_books.merge(find_upvoted_by(user))
  end

  def reset_last_read
    user.feed_read_at = nil
  end

  def find_by_author(authors)
    collection = []
    authors.each do |author|
      collection << books.select { |book| book.author == author }
    end
    collection.flatten
  end

  def find_upvoted_by(user)
    Upvote.all.select { |upvote| upvote.user == user }.map(&:book)
  end

  private

    def books
      $dataset.books
    end

end
