class Seed

  attr_reader :users, :authors, :follows, :books, :upvotes

  def initialize
    @users = seed(User, 10)
    @authors = seed(Author, 10)
    @follows = seed_follows
    @books = seed_books
    @upvotes = seed_upvotes
  end

  def seed(type, amount)
    collection = []
    amount.times do
      collection << type.new(Faker::Name.name)
    end
    collection
  end

  def seed_follows
    follows = []
    10.times do
      user = self.users.sample
      author = self.authors.sample
      user.add_author(author)
      follows << Follow.new(user, author)
    end
    follows
  end

  def seed_books
    books = []
    10.times do
      books << Book.new(self.authors.sample, Faker::Name.title, Faker::Date.backward(14))
    end
    books
  end

  def seed_upvotes
    collection = []
    10.times do
      collection << Upvote.new(self.users.sample, self.books.sample)
    end
    collection
  end
end
