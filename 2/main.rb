require 'faker'
require 'set'

require_relative 'models/author'
require_relative 'models/base_user'
require_relative 'models/book'
require_relative 'models/follow'
require_relative 'models/upvote'
require_relative 'models/user'

require_relative 'services/feed'
require_relative 'services/seed'

$dataset = Seed.new


user = $dataset.users.sample
author = $dataset.authors.sample

@feed = Feed.new(user)

puts "Unread count: #{@feed.unread_count}"

@data = @feed.retrieve

puts "Feed:"
@data.each do |item|
  puts "#{item.published_on} - #{item.author.name}: #{item.title}"
end

puts "Add book"

$dataset.books << Book.new(author, Faker::Name.title, Date.today)
user.add_author(author) # NOTE: that should actually show old books after refresh too!

$dataset.books << Book.new(author, Faker::Name.title, Date.today)
user.add_author(author)

puts "Refresh:"
@data = @feed.refresh

@data.each do |item|
  puts "#{item.published_on} - #{item.author.name}: #{item.title}"
end

puts "Reset last read"
@feed.reset_last_read

puts "Unread count: #{@feed.unread_count}"

@data = @feed.refresh

puts "Refresh after reset:"

@data.each do |item|
  puts "#{item.published_on} - #{item.author.name}: #{item.title}"
end

# There is kind of bug because when we add new author for user then refresh method should get not only new books for him, that should get all books (?)
