class Upvote

  attr_reader :user, :book

  def initialize(user, book)
    @user = user
    @book = book
  end

  def self.all
    $dataset.upvotes
  end
end
