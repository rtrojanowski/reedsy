class User < BaseUser

  attr_reader :authors
  attr_accessor :feed_read_at

  def initialize(name)
    super
    @authors = []
  end

  def add_author(author)
    @authors << author unless @authors.include?(author)
  end
end
