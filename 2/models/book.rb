class Book

  attr_reader :author, :title, :published_on

  def initialize(author, title, published_on)
    @author = author
    @title = title
    @published_on = published_on
  end
end

